package com.cognizant.retailmate.loyalty.loyaltypoints1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments.DetailsFragment;
import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments.HistoryFragment;
import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments.RedeemFragment;
import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments.ShowPopFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 543898 on 1/23/2017.
 */

public class DetailedLoyaltyActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        DetailedLoyaltyActivity.ViewPagerAdapter adapter = new DetailedLoyaltyActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RedeemFragment(), getResources().getString(R.string.redeem));
        adapter.addFragment(new HistoryFragment(), getResources().getString(R.string.history));
        adapter.addFragment(new DetailsFragment(), getResources().getString(R.string.details));
        viewPager.setAdapter(adapter);
    }

    public void showRedeemPopup(View view) {

        Intent intent = new Intent(DetailedLoyaltyActivity.this, ShowPopFragment.class);
        startActivity(intent);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}