package com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cognizant.retailmate.loyalty.loyaltypoints1.R;

/**
 * Created by 543898 on 12/28/2016.
 */

public class ShowPopFragment extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_up);
    }

    public void closePopup(View view) {
        finish();
    }

    public void showCart(View view) {
    }

}