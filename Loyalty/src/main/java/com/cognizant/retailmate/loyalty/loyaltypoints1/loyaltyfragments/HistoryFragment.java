package com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.retailmate.loyalty.loyaltypoints1.R;
import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltymodel.LoyaltyResponseModel;
import com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltyrecycview.HistoryAdapter;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 543898 on 1/23/2017.
 */
public class HistoryFragment extends Fragment {


    RecyclerView recyclerView;

    Gson gson;

    LoyaltyResponseModel loyaltyResponseModel;

    HistoryAdapter recyclerView_Adapter;

    RecyclerView.LayoutManager recyclerViewLayoutManager;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson=new Gson();
        loyaltyResponseModel = gson.fromJson(loadJSONFromAsset(), LoyaltyResponseModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View historyLayout = inflater.inflate(R.layout.history, container, false);


        recyclerView = (RecyclerView) historyLayout.findViewById(R.id.recycler_view2);

        recyclerViewLayoutManager = new GridLayoutManager(historyLayout.getContext(), 1);

        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        Log.e("####", loyaltyResponseModel.getLoyaltyTransactionDetail().size() + "__");

        recyclerView_Adapter = new HistoryAdapter(historyLayout.getContext(), loyaltyResponseModel);

        recyclerView.setAdapter(recyclerView_Adapter);

        return historyLayout;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getApplicationContext().getAssets().open("response.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
