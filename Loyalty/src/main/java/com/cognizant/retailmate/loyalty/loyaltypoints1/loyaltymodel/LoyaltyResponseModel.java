
package com.cognizant.retailmate.loyalty.loyaltypoints1.loyaltymodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoyaltyResponseModel {

    @SerializedName("LoyaltyTransactionDetail")
    @Expose
    private List<LoyaltyTransactionDetail> loyaltyTransactionDetail = null;
    @SerializedName("TierPoint")
    @Expose
    private String tierPoint;
    @SerializedName("ReedemptionPoint")
    @Expose
    private String reedemptionPoint;

    public List<LoyaltyTransactionDetail> getLoyaltyTransactionDetail() {
        return loyaltyTransactionDetail;
    }

    public void setLoyaltyTransactionDetail(List<LoyaltyTransactionDetail> loyaltyTransactionDetail) {
        this.loyaltyTransactionDetail = loyaltyTransactionDetail;
    }

    public String getTierPoint() {
        return tierPoint;
    }

    public void setTierPoint(String tierPoint) {
        this.tierPoint = tierPoint;
    }

    public String getReedemptionPoint() {
        return reedemptionPoint;
    }

    public void setReedemptionPoint(String reedemptionPoint) {
        this.reedemptionPoint = reedemptionPoint;
    }

}
